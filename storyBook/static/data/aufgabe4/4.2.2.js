const ul = document.querySelector('ul')
const input = document.querySelector('input')
const btn = document.querySelector('button')

btn.onclick = function () {

    const listItem = document.createElement("li")
    listItem.setAttribute("id", input.value)
    ul.appendChild(listItem)
    TimeKeeper(input.value)
}

function TimeKeeper(id) {
    let running = true
    let startTime;
    if (startTime == undefined) {
        startTime = new Date();
    }
    window.setInterval(() => setTime(), 1000)
    function setStopForTimeKeeper() {
        
        currentdate = new Date();
        let takenTime = currentdate - startTime
        let time = new Date(takenTime)

        let timeToShow = time.getHours() - 1 + ":" +
            time.getMinutes() + ":" +
            time.getSeconds()
        return timeToShow
    }

    function setPauseForTimeKeeper(){
        currentdate = new Date();
        let takenTime = currentdate - startTime
        let time = new Date(takenTime)

        let timeToShow = time.getHours() - 1 + ":" +
        time.getMinutes() + ":" +
        time.getSeconds()
        return timeToShow

    }

    function setTime() {
        if (running) {
            result = setStopForTimeKeeper();
            document.getElementById(id).innerHTML = `${id} ${result} <button> Stop </button>`
            
            let stopBtn = document.getElementById(id).querySelector("button")
            stopBtn.onclick = () => {
                running = false 
            }

        }
    }

}