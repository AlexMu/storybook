const ul = document.querySelector('ul')
const input = document.querySelector('input')
const btn = document.querySelector('button')

btn.onclick = function () {
    const listItem = document.createElement("li")
    listItem.innerHTML = `${input.value} <button> Delete </button>`
    const delete_button = listItem.querySelector("button")
    delete_button.onclick = () => ul.removeChild(listItem)
    ul.appendChild(listItem)
    input.value = ""
    input.focus();
}