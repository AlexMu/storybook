<template>
    <div class="counter">
        <form  v-on:input= "onInput">
        <input v-model= "text"    type= "text" id="input">
        <output  type="text" for="input">{{letters}}  Letters</output>
        <output type="text" for="input">{{whiteSpaces}} White Spaces</output>
        <output type="text" for="input">{{words}} Words</output>
        </form>
        <textarea v-model="text" ></textarea>
    </div>
</template>

<script>

export default {
  name: 'counter',
  data () {
      return {
          text: "Something",
          letters: 0,
          whiteSpaces:0,
          words:0
      }
  },

  methods: {
      onInput: function() {
          let letterCount=0;
          let whiteSpaceCound=0;
          let wordCount=0;
          let charBefore = " ";
          this.text.split('').forEach((char) =>{
              if(char == " "){
                  whiteSpaceCound++
                  if(charBefore != " "){
                      wordCount++
                  }
              }else{
                  letterCount++
              }

              charBefore = char              
          })
          if(charBefore != " "){
              wordCount++
          }

          this.letters = letterCount;
          this.whiteSpaces = whiteSpaceCound;
          this.words = wordCount;
        
      },
      
  }
 
}
</script>

<style>

</style>