

NavBarHeader.vue
<template>
    <div class="navHeadbar"> <NavBarHeaderButton v-for=" element in aufgaben" v-bind:key="element.nr" :name="element.aufgabe" :setSelectedTask="setSelectedTask" :nr="element.nr" /></div>
</template>

<script>
import NavBarHeaderButton from '@/components/NavBarHeaderButton.vue'
import Data from '@/Data.js'
export default {
  name: 'NavBarHeader',
  components: {
    NavBarHeaderButton
  },
  props:{
      setSelectedTask: Function
  },
  computed:{
      aufgaben(){
          return Data.aufgaben
      }
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style>
.navHeadbar{   
  height: auto;
  width: 98%;
  display: flex;
  align-self: center;
  flex-wrap: wrap;
}
</style>


NavBarHeaderButton.vue

<template>
    <div class="navBarHeaderButton" @click="setSelectedTask(nr)"> <p>{{name}}</p></div>
</template>

<script>
export default {
  name: 'navBarHeaderButton',
  props: {
      name: String,
      setSelectedTask: Function,
      nr: Number
    }
}
</script>


<style>
.navBarHeaderButton{   
  height: 50px;
  min-width: 90px;
  align-content: center;
  justify-content: center;
  display: flex;
  border:solid;
  border-radius: 10px;
  margin-right: 5px;
  margin-bottom: 5px;
  cursor: pointer;
}
</style>