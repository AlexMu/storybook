const cachedWebsite = "cachedWebsite-v1"
const assets = [
  "/",
  "/index.html",
 "/data.json",
 "https://unpkg.com/@webcomponents/webcomponentsjs@latest/webcomponents-loader.js"
  
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(cachedWebsite).then(cache => {
      cache.addAll(assets)
    })
  )
})

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
      caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)
      })
    )
  })