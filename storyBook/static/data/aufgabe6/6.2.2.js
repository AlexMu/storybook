#!/usr/bin/env node
var fs = require('fs');
var readline = require('readline')

const startTime = new Date();

var fileWriter = fs.createWriteStream('mergedFiles.txt')



const filesName1 = process.argv[2];
const filesName2 = process.argv[3];  

 function mergeTwoFiles(filesName1, filesName2){

    var lines1 = []
    var lines2 = []
    var file1Stream = fs.createReadStream(filesName1, "utf8");
    var file2Stream = fs.createReadStream (filesName2, 'utf8');
    const lineReader1 = readline.createInterface({
        input: file1Stream
    })
    const lineReader2 = readline.createInterface({
        input: file2Stream
    })

    
    lineReader1.on('line', (line)=> {
        lines1.push(line)
        })

        var counter = 0;
    
    lineReader2.on('line', (line)=> {
        fileWriter.write( lines1[counter] + line +  '\n');
        counter++
            })

    lineReader2.on('close', function() {
            

            const endTime = new Date();
            fileWriter.write( "readFileStream needed " + (endTime - startTime) + " ms")
          })

          
   
    
}




mergeTwoFiles(filesName1, filesName2);