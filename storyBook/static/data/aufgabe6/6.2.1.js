#!/usr/bin/env node
var fs = require('fs');

const startTime = new Date();

var fileWriter = fs.createWriteStream('syncMergedFiles.txt')

const filesName1 = process.argv[2];
const filesName2 = process.argv[3];  

function mergeTwoFiles(filesName1, filesName2){

    var file1 = fs.readFileSync(filesName1, 'utf8');
    var file2 = fs.readFileSync(filesName2, 'utf8');
    let linesOf1 = file1.split('\n')
    let linesOf2 = file2.split('\n')

    linesOf1.forEach((line, index) => {
        fileWriter.write(  line + linesOf2[index] + '\n')
    })
        console.log("Merged " + filesName1+ " and "+filesName2)
        
        const endTime = new Date();
        fileWriter.write( "readFileSync needed " + (endTime - startTime) + " ms")
   
    
}


mergeTwoFiles(filesName1, filesName2);