function applyf(f){
	return (x) => {
  	return (y) => {
    	return f(x)(y);
    }
  }
}