function demethodize(f) {
    return (x, y) => {
        return f(y);
    }
}