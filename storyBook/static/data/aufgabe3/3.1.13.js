function revocable(f){
	let isRevoked = false;
  console.log(isRevoked)
	return {
  	invoke: (x)=>{ if(!isRevoked){return f(x)}else{ throw new Error('Was revoked!') }},
    revoke: ()=>{  isRevoked = true}
  }
}