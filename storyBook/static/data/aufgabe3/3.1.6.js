function methodize(f) {
    return (x) => {
        return f(this, x)
    }
}