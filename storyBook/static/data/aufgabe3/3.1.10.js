function composeb(func1, func2){
	return (x, y, z)=>{
  	return func2(func1(x,y), z)
  }
}