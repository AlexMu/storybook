function counterf(x){
	let count = x;
  return {inc: ()=>{return ++count},
  				dec: ()=>{return --count}}

}