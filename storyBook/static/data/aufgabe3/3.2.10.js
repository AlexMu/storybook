function unarymf(func, sym){
    return (x)=>{
          if(typeof x === "number"){
            x = {value: x, source: x};
        }
        
        var result = func(x.value);
        return {
            value: result,
          source: "("+ sym + x.source +")"
        }
      }
    }