function unaryc(func){
	return (x, callback)=>{
  	callback(func(x));
  }
}