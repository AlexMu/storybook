function binarymf(func, sym){

	return (x,y)=>{
  	if(typeof x === "number"){
    	x = {value: x, source: x};
    }
    if(typeof y === "number"){
    	y = {value: y, source:y}
    }
    var result = func(x.value, y.value);
    return {
    	value: result,
      source: "("+x.source + sym + y.source +")"
    }
  }
}