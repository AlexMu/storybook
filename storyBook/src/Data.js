var aufgabe1= {
    "aufgabe": "Aufgabe 1",
    "nr": 1,
    "unteraufgaben": [
        {
            "unteraufgabenNr": 1,
            "title": "1.1 Fachliche Argumentation über Erfolgsprinzipien des WWW",
            "aufgabenMitLoesung": [
                {
                    "text": "Drei Fragen zum WWW",
                    "path": "static/data/aufgabe1/1.1.html",
                    "showAsCode": 0
                }
            ]
        },
        {
            "unteraufgabenNr": 2,
            "title": "1.2 HTTP",
            "aufgabenMitLoesung": [
                {
                    "text": "Fünf Fragen zu HTTP",
                    "path": "static/data/aufgabe1/1.2.html",
                    "showAsCode": 0
                }
            ]
        },
        {
            "unteraufgabenNr": 3,
            "title": "1.3 Wireframe with HTML and CSS: Tribute Page",
            "aufgabenMitLoesung": [
                {
                    "text": "Tribute Page",
                    "path": "static/data/aufgabe1/1.3.html",
                    "showAsCode": 1
                }
            ]
        },
        {
            "unteraufgabenNr": 4,
            "title": "1.4. Wireframe with HTML and CSS (Survey Form)",
            "aufgabenMitLoesung": [
                {
                    "text": "HTML-Wireframe (Survey Form)",
                    "path": "static/data/aufgabe1/1.4.html",
                    "showAsCode": 1
                }
            ]
        },
        {
            "unteraufgabenNr": 5,
            "title": "1.5. Wireframe with HTML and CSS (Product Landing Page)",
            "aufgabenMitLoesung": [
                {
                    "text": "HTML-Wireframe (Product Landing Page)",
                    "path": "static/data/aufgabe1/1.5.html",
                    "showAsCode": 1
                }
            ]
        }
    ]
}

var aufgabe2= {   
    "aufgabe": "Aufgabe 2",
    "nr": 2,
    "unteraufgaben": [
         {
             "unteraufgabenNr":1,
             "title":"2.1. Responsiv mit Flexbox Desktop-First",
             "aufgabenMitLoesung": [
                 {"text": `Spielen Sie zunächst das Flexbox Froggy-Spiel, um Flexbox zu lernen. Implementieren Sie dann ausschließlich mit HTML und CSS Flexbox folgendes responsive Webdesign nach der Desktop-First-Strategie!`,
                  "path": "static/data/aufgabe2/2.1.html",
                  "showAsCode": 1},
             ]
         },
         {
            "unteraufgabenNr":2,
            "title":"2.2. Responsiv mit Grid Mobile-First",
            "aufgabenMitLoesung": [
                {"text": `Spielen Sie zunächst das Grid Garden -Spiel, um Grid Layout zu lernen. Implementieren Sie dann das gleiche responsive Webdesign wie in Aufgabe 2.1 allerdings mit Grid und der Mobile-First-Strategie! Vermeiden Sie diesmal außerdem das Erscheinen von Scrollbars.`,
                 "path": "static/data/aufgabe2/2.2.html",
                 "showAsCode": 1},
            ]
        },
        {
            "unteraufgabenNr":3,
            "title":"2.3. Holy Grail",
            "aufgabenMitLoesung": [
                {"text": `Implementieren Sie ausschließlich mit HTML und CSS folgendes responsive Webdesign. Vermeiden Sie das Erscheinen von Scrollbars so weit wie möglich. Andererseits sollten der Viewport sowohl horizontal als auch vertikal zu 100% genutzt werden, d.h. vermeiden Sie weiße Flächen. Implementieren Sie dies mit Flexbox-Layout. Geben Sie hier HTML- und CSS-Code zusammen ein:`,
                 "path": "static/data/aufgabe2/2.3.1.html",
                 "showAsCode": 1},
                 {"text": `Implementieren Sie ausschließlich mit HTML und CSS folgendes responsive Webdesign. Vermeiden Sie das Erscheinen von Scrollbars so weit wie möglich. Andererseits sollten der Viewport sowohl horizontal als auch vertikal zu 100% genutzt werden, d.h. vermeiden Sie weiße Flächen. Implementieren Sie dies mit Grid-Layout. Geben Sie hier HTML- und CSS-Code zusammen ein:`,
                 "path": "static/data/aufgabe2/2.3.2.html",
                 "showAsCode": 1},
            ]
        },
        {
            "unteraufgabenNr":4,
            "title":"2.4. Responsiv mit Grid",
            "aufgabenMitLoesung": [
                {"text": `Implementieren Sie folgende Landing Page responsiv mit Grid Layout. Vermeiden Sie außerdem das Erscheinen von Scrollbars so weit wie möglich.`,
                 "path": "static/data/aufgabe2/2.4.html",
                 "showAsCode": 1},
            ]
        },
     ]
 };

 var aufgabe3 = {   
    "aufgabe": "Aufgabe 3",
    "nr": 3,
    "unteraufgaben": [
        {
            "unteraufgabenNr":1,
            "title":"Aufgabe 3.1: Funktionen in JavaScript",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie eine Funktion identity_function(), die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt.`,
                 "path": "static/data/aufgabe3/3.1.1.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Addier-Funktion addf(), so dass addf(x)(y) genau x + y zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. addf(x) liefert eine Funktion, die auf y angewandt wird.)`,
                 "path": "static/data/aufgabe3/3.1.2.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion applyf(), die aus einer binären Funktion wie add(x,y) eine Funktion addfberechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. addf = applyf(add); addf(x)(y) soll add(x,y) liefern. Entsprechend applyf(mul)(5)(6) soll 30 liefern, wenn mul die binäre Multiplikation ist.`,
                 "path": "static/data/aufgabe3/3.1.3.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion curry() (von Currying), die eine binäre Funktion und ein Argument nimmt, um daraus eine Funktion zu erzeugen, die ein zweites Argument entgegen nimmt, z.B. add3 = curry(add, 3);add3(4) ergibt 7. curry(mul, 5)(6) ergibt 30.`,
                 "path": "static/data/aufgabe3/3.1.4.js",
                 "showAsCode": 0},
                 {"text": `Erzeugen Sie die inc-Funktion mit Hilfe einer der Funktionen addf, applyf und curry aus den letzten Aufgaben, ohne die Funktion inc() selbst zu implementieren. (inc(x) soll immer x + 1 ergeben und lässt sich natürlich auch direkt implementieren. Das ist aber hier nicht die Aufgabe.) Vielleicht schaffen Sie es auch, drei Varianten der inc()-Implementierung zu schreiben?`,
                 "path": "static/data/aufgabe3/3.1.5.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion methodize(), die eine binäre Funktion (z.B. add, mul) in eine unäre Methode verwandelt. Nach Number.prototype.add = methodize(add); soll (3).add(4) genau 7 ergeben.`,
                 "path": "static/data/aufgabe3/3.1.6.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion demethodize(), die eine unäre Methode (z.B. add, mul) in eine binäre Funktion umwandelt. demethodize(Number.prototype.add)(5, 6) soll 11 ergeben.`,
                 "path": "static/data/aufgabe3/3.1.7.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion twice(), die eine binäre Funktion in eine unäre Funktion umwandelt, die den einen Parameter zweimal weiter reicht. Z.B. var double = twice(add); double(11) soll 22 ergeben; var square = twice(mul); square(11) soll mul(11,11) === 121 ergeben.`,
                 "path": "static/data/aufgabe3/3.1.8.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion composeu(), die zwei unäre Funktionen in eine einzelne unäre Funktion transformiert, die beide nacheinander aufruft, z.B. soll composeu(double, square)(3) genau 36 ergeben.`,
                 "path": "static/data/aufgabe3/3.1.9.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion composeb(), die zwei binäre Funktionen in eine einzelne Funktion transformiert, die beide nacheinander aufruft, z.B. composeb(add, mul)(2, 3, 5) soll 25 ergeben.`,
                 "path": "static/data/aufgabe3/3.1.10.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Funktion once(), die einer anderen Funktion nur einmal erlaubt, aufgerufen zu werden, z.B. add_once = once(add); add_once(3, 4) soll beim ersten Mal 7 ergeben, beim zweiten Mal soll jedoch add_once(3, 4) einen Fehlerabbruch bewirken.`,
                 "path": "static/data/aufgabe3/3.1.11.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine Fabrik-Funktion counterf(), die zwei Funktionen inc() und dec() berechnet, die einen Zähler hoch- und herunterzählen. Z.B. counter = counterf(10); Dann soll counter.inc() 11 und counter.dec() wieder 10 ergeben.`,
                 "path": "static/data/aufgabe3/3.1.12.js",
                 "showAsCode": 0},
                 {"text": `Schreiben Sie eine rücknehmbare Funktion revocable(), die als Parameter eine Funktion nimmt und diese bei Aufruf ausführt. Sobald die Funktion aber mit revoke() zurück genommen wurde, führt ein erneuter Aufruf zu einem Fehler. Z.B. temp = revocable(alert); temp.invoke(7); // führt zu alert(7); temp.revoke(); temp.invoke(8); // Fehlerabbruch!`,
                 "path": "static/data/aufgabe3/3.1.13.js",
                 "showAsCode": 0},
                 {"text": `Implementieren Sie ein "Array Wrapper"-Objekt mit den Methoden get, store und append, so dass ein Angreifer keinen Zugriff auf das innere, private Array hat.`,
                 "path": "static/data/aufgabe3/3.1.14.js",
                 "showAsCode": 0},
            ]
        },
        {
            "unteraufgabenNr":2,
            "title":"Aufgabe 3.2: Advanced Functional JavaScript Programming",
            "aufgabenMitLoesung": [
                {"text": `Make a function that makes a publish/subscribe object. It will reliably deliver all publications to all subscribers in the right order.
                my_pubsub = pubsub();
                my_pubsub.subscribe(alert);
                my_pubsub.publish("It works!"); // alert("It works!") `,
                 "path": "static/data/aufgabe3/3.2.1.js",
                 "showAsCode": 0},
                 {"text": `Make a factory that makes functions that generate unique symbols.
                 gensym = gensymf('G');
                 gensym() // 'G0'
                 gensym() // 'G1'
                 gensym() // 'G2'
                 gensym() // 'G3' `,
                 "path": "static/data/aufgabe3/3.2.2.js",
                 "showAsCode": 0},
                 {"text": `Make a function that returns a function that will return the next fibonacci number.
                 var fib = fibonaccif(0, 1);
                 fib() // 0
                 fib() // 1
                 fib() // 1
                 fib() // 2
                 fib() // 3
                 fib() // 5 `,
                 "path": "static/data/aufgabe3/3.2.3.js",
                 "showAsCode": 0},
                 {"text": `Write a function that adds from many invocations, until it sees an empty invocation.
                 addg(3)(4)(5)() // 12
                 addg(1)(2)(4)(8)() // 15 `,
                 "path": "static/data/aufgabe3/3.2.4.js",
                 "showAsCode": 0},
                 {"text": `Write a function that will take a binary function and apply it to many invocations.
                 applyg(add)(3)(4)(5)() // 12
                 applyg(add)(1)(2)(4)(8)() // 15 `,
                 "path": "static/data/aufgabe3/3.2.5.js",
                 "showAsCode": 0},
                 {"text": `Write a function m that takes a value and an optional source string and returns them in an object.
                 JSON.stringify(m(1)) // {"value": 1, "source": "1"}
                 JSON.stringify(m(Math.PI, "pi")) // {"value": 3.14159..., "source": "pi"} `,
                 "path": "static/data/aufgabe3/3.2.6.js",
                 "showAsCode": 0},
                 {"text": `Write a function addm that takes two m objects and returns an m object.
                 JSON.stringify(addm(m(3), m(4))) // {"value": 7, "source": "(3+4)"} `,
                 "path": "static/data/aufgabe3/3.2.7.js",
                 "showAsCode": 0},
                 {"text": `Write a function binarymf that takes a binary function and a string and returns a function that acts on m objects.
                 addm = binarymf(add, "+");
                 JSON.stringify(addm(m(3), m(4))) // {"value": 7, "source": "(3+4)"} `,
                 "path": "static/data/aufgabe3/3.2.8.js",
                 "showAsCode": 0},
                 {"text": `Modify function binarymf so that the functions it produces can accept arguments that are either numbers or m objects.
                 addm = binarymf(add, "+");
                 JSON.stringify(addm(3, 4)) // {"value": 7, "source": "(3+4)"} `,
                 "path": "static/data/aufgabe3/3.2.9.js",
                 "showAsCode": 0},
                 {"text": `Write function unarymf, which is like binarymf except that it acts on unary functions.
                 squarem = unarymf(square, "square");
                 JSON.stringify(squarem(4)) // {"value": 16, "source": "(square 4)"} `,
                 "path": "static/data/aufgabe3/3.2.10.js",
                 "showAsCode": 0},
                 {"text": `Write a function that takes the lengths of two sides of a triangle and computes the length of the hypotenuse. (Hint: c2 = a2 + b2)
                 hyp(3, 4) // 5 `,
                 "path": "static/data/aufgabe3/3.2.11.js",
                 "showAsCode": 0},
                 {"text": `Write a function that evaluates array expressions.
                 hypa = [ Math.sqrt, [ add, [mul, 3, 3], [mul, 4, 4] ] ];
                 exp(hypa) // 5 `,
                 "path": "static/data/aufgabe3/3.2.12.js",
                 "showAsCode": 0},
                 {"text": `Make a function that stores a value in a variable.
                 var variable; store(5); // variable === 5 `,
                 "path": "static/data/aufgabe3/3.2.13.js",
                 "showAsCode": 0},
                 {"text": `Make a function that takes a binary function, two functions that provide operands, and a function that takes the result.
                 quatre( add, identityf(3), identityf(4), store ); // variable === 7 `,
                 "path": "static/data/aufgabe3/3.2.14.js",
                 "showAsCode": 0},
                 {"text": `Make a function that takes a unary function, and returns a function that takes an argument and a callback.
                 sqrtc = unaryc(Math.sqrt); sqrt(81, store) // variable === 9 `,
                 "path": "static/data/aufgabe3/3.2.15.js",
                 "showAsCode": 0},
                 {"text": `Make a function that takes a binary function, and returns a function that takes two arguments and a callback.
                 addc = binaryc(add); addc(4, 5, store) // variable === 9
                 mulc = binaryc(mul); mulc(2, 3, store) // variable === 6 `,
                 "path": "static/data/aufgabe3/3.2.16.js",
                 "showAsCode": 0},
            ]
        }
     ]
 };

 var aufgabe4 = {   
    "aufgabe": "Aufgabe 4",
    "nr": 4,
    "unteraufgaben": [
         {
             "unteraufgabenNr":1,
             "title":"4.1. Einkaufsliste",
             "aufgabenMitLoesung": [
                 {"text": `Implementieren Sie die interaktive Anwendung "Einkaufsliste" selbstständig in JavaScript durch Nutzung der DOM API. Jeder Punkt auf der Einkaufsliste soll sich individuell löschen lassen. Suchen Sie eine möglichst kurze und elegante Lösung. Denken Sie außerdem über Usability nach: Welche User Interaktionen sollten ebenfalls erlaubt sein? Implementieren Sie dann auch diese. Geben Sie die komplette HTML-Seite exkl. JavaScript-Quelltext an. Schreiben Sie Ihren JavaScript-Quelltext in eine separate Datei.`,
                 "path": "static/data/aufgabe4/4.1.1.html",
                 "showAsCode": 1},
                 
             ]

         },
         {
            "unteraufgabenNr":2,
            "title":"4.2. Rednerliste mit Zeitmessung",
            "aufgabenMitLoesung": [
                {"text": `Implementieren Sie die interaktive Anwendung "Rednerliste mit Zeitmessung" selbstständig in JavaScript durch Nutzung der DOM API und der Timer-Funktionen. Neue Redner sollen auf Knopfdruck hinzugefügt werden können. Deren Uhr wird dann sofort automatisch gestartet und alle anderen Uhren angehalten. Bei jedem Redner soll die individuelle, gemessene Redezeit sekundengenau angezeigt werden. Für jeden Redner soll es einen eigenen Start-/Stopp-Button geben. Es soll immer nur eine Uhr laufen. Angezeigt werden sollen die bisherigen Summenzeiten aller Redebeiträge der betreffenden Person. Suchen Sie eine möglichst kurze und elegante Lösung. Achten Sie gleichzeitig auf gute Usability: z.B. wenn die Eingabe mit einem Return beendet wird, soll der Button-Click nicht mehr erforderlich sein, usw. Geben Sie die komplette HTML-Seite exkl. JavaScript-Quelltext an. Schreiben Sie Ihren JavaScript-Quelltext in eine separate Datei.`,
                "path": "static/data/aufgabe4/4.2.1.html",
                "showAsCode": 1},
                
            ]

        },
        {
            "unteraufgabenNr":3,
            "title":"4.3. Tabellenkalkulation mit den Bordmitteln des Webs",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie eine Tabellenkalkulation mit den Bordmitteln des Webs. In die Tabelle soll man Zahlen und Formeln eintragen können. Die Formeln sollen berechnet werden, sobald sie fertig eingegeben sind, aber auch immer wieder editiert werden können. Beginnen Sie mit einfachen Summenformeln wie =SUM(A2:D2). Wenn man das Formel-Feld verlässt, soll das berechnete Ergebnis angezeigt werden: Wenn man an den Summanden etwas ändert, soll die Summe automatisch neu berechnet und neu angezeigt werden, wie man es von den üblichen Tabellenkalkulationsprogrammen wie Excel gewohnt ist. (Die Tabelle soll natürlich beliebig groß sein können. Die Größe dürfen Sie durch Parameter festgelegen. Die dynamische Vergrößerung kann erst einmal vernachlässigt werden.) Geben Sie die komplette HTML-Seite exkl. JavaScript-Quelltext an. Schreiben Sie Ihren JavaScript-Quelltext in eine separate Datei. `,
                "path": "static/data/aufgabe4/4.3.1.html",
                "showAsCode": 1},
                
            ]

        },
        {
            "unteraufgabenNr":4,
            "title":"4.4. HTML-Editor",
            "aufgabenMitLoesung": [
                {"text": `Arbeiten Sie das Tutorial Create a WYSIWYG Editor With the contentEditable Attribute durch und erstellen Sie Ihren eigenen HTML-Editor.

                (Optional: Versuchen Sie dabei ohne Frameworks auszukommen, also ohne jQuery etc. Arbeiten Sie nur mit Vanilla JS.) Geben Sie die komplette HTML-Seite exkl. JavaScript-Quelltext an. Schreiben Sie Ihren JavaScript-Quelltext in eine separate Datei.`,
                "path": "static/data/aufgabe4/4.4.1.html",
                "showAsCode": 1},
               
            ]

        }
     ]
 };

 var aufgabe5 = {"aufgabe": "Aufgabe 5",
    "nr": 5,
    "unteraufgaben": [
        {
            "unteraufgabenNr":1,
            "title":"5.1. Promises",
            "aufgabenMitLoesung": [
                {"text": `Erstellen Sie auf Ihrem Server www2.inf.h-brs.de (oder localhost) zwei Text-Dateien A.txt und B.txt mit ungefähr gleich vielen Zeilen. Laden Sie mit der fetch-API parallel beide Text-Dateien vom Server. Geben Sie auf einer Webseite den Inhalt beider Dateien zeilenweise aus, wobei der Anfang der Zeile aus A.txt und das Ende aus B.txt stammen soll. Die beiden Dateien sollen also zeilenweise konkateniert werden. Erzielen Sie max. Geschwindigkeit durch maximale Parallelität. Achten Sie gleichzeitig auf Korrektheit. Verwenden Sie dabei ausschließlich die Promise API ohne async / await.`,
                "path": "static/data/aufgabe5/5.1.html",
                "showAsCode": 1},
                
            ]
        },
        {
            "unteraufgabenNr":2,
            "title":"5.2. async / await",
            "aufgabenMitLoesung": [
                {"text": `Lösen Sie die letzte Aufgabe mit async / await statt Promise.`,
                "path": "static/data/aufgabe5/5.2.html",
                "showAsCode": 1},
                
            ]
        },
        {
            "unteraufgabenNr":3,
            "title":"5.3. Web Worker",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie eine Webseite, die Primzahlen berechnet und fortlaufend neu berechnete Primzahlen hinzufügt. Verwenden Sie dabei die BigNum-Arithmetik. Auf der Webseite soll außerdem ein Ladebalken ständig hin- und herlaufen, damit man feststellen kann, ob die Anzeige ruckelfrei abläuft. Stellen Sie fest, ab welcher Zahl der Ladebalken anfängt zu ruckeln.

                Schreiben Sie dann einen Web Worker, der Primzahlen berechnet und diese mittels postMessage an die EventLoop in der Webseite sendet, damit diese dort angezeigt werden können.`,
                "path": "static/data/aufgabe5/5.3.html",
                "showAsCode": 1},
                
            ]
        },
        {
            "unteraufgabenNr":4,
            "title":"5.4. WWW-Navigator",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie einen Navigator für die Fachbegriffe des WWW zu den Oberthemen HTML, CSS und JavaScript. Im Hauptmenü sollen diese 3 Oberthemen zur Auswahl stehen. Im Untermenü soll eine zugehörige Liste von Fachbegriffen zum jeweiligen ausgewählten Oberthema stehen. In der Mitte soll eine Dokumentation zum ausgewählten Fachbegriff erscheinen mit Hyperlinks zu den anderen Fachbegriffen. Wird auf einen solchen Hyperlink geklickt, so sollen sich auch die beiden Menüs anpassen. Mit dem Back-Button des Browsers soll ein Zurücksprung möglich sein.`,
                "path": "static/data/aufgabe5/5.4.html",
                "showAsCode": 1},
                
            ]
        },
    ]
} ;

 var aufgabe6 = {
     "aufgabe": "Aufgabe 6",
     "nr": 6,
     "unteraufgaben": [
        {
            "unteraufgabenNr":1,
            "title":"6.1. File Generatoren",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie in Node.js zwei Kommandozeilen-Tools, um große Dateien zu erzeugen.

                1. node number_file_gen.js 20_000
                
                soll eine Datei mit 20.000 Zeilen erzeugen. In jeder Zeile soll die Zeilennummer und ein Punkt stehen. 20.000 ist dabei ein Parameter des Tools. Jede andere Zahl soll ebenfalls erlaubt sein.
                
                2. node alpha_file_gen.js 123456
                
                soll eine Datei mit 123456 Zeilen erzeugen. In jeder Zeile soll eine Kombination von Großbuchstaben (ohne Umlaute) in lexikographischer Reihenfolge stehen. Die Datei beginnt also mit:
                
                A
                B
                C
                ...
                Z
                AA
                AB
                AC
                AD
                ...
                
                usw.
                
                Implementieren Sie Ihre Kommandozeilen-Tools in modernem ECMAScript 2020.
                
                Geben Sie hier Ihren ECMAScript 2020-Quellcode für number_file_gen.js ein:`,
                "path": "static/data/aufgabe6/6.1.1.js",
                "showAsCode": 0},
                {
                 "text": "Geben Sie hier Ihren ECMAScript 2020-Quellcode für alpha_file_gen.js ein:",
                 "path": "static/data/aufgabe6/6.1.2.js",
                 "showAsCode": 0   
                }
                
            ]
        },
        {
            "unteraufgabenNr":2,
            "title":"6.2. Performance Merge",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie in Node.js zwei Programme merge_files.js und merge_streams.js, um große Dateien zu zeilenweise zusammenzuführen, merge_files.js mit fs.readFile und merge_streams.js mit Streams, also createReadFileStream und pipeline.

                1. node merge_files.js big_file1.txt big_file2.txt
                2. node merge_streams.js big_file1.txt big_file2.txt
                
                Implementieren Sie Ihre beiden Programme merge_files.js und merge_streams.js in modernem ECMAScript 2020.
                
                Messen Sie anschließend die Performanz beider Programme. Geben Sie hier Ihren ECMAScript 2020-Quellcode für merge_files.js ein:`,
                "path": "static/data/aufgabe6/6.2.1.js",
                "showAsCode": 0},
                {
                 "text": "Geben Sie hier Ihren ECMAScript 2020-Quellcode für merge_streams.js ein:",
                 "path": "static/data/aufgabe6/6.2.2.js" ,
                 "showAsCode": 0  
                }
                
            ]
        },
        {
            "unteraufgabenNr":3,
            "title":"6.3. Express.js Server",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie einen lokalen Express.js-HTTP-Server für den Merge-Dienst, d.h. auf der lokal angezeigten Webseite kann man zwei Dateien auswählen, um diese zeilenweise zu mergen. Das Ergebnis soll dann in der Webseite erscheinen und zum Download angeboten werden.

                Arbeiten Sie lokal auf Ihrem Notebook mit localhost. Der Web-Server des Fachbereichs www2.inf.h-brs.de bietet keinen Node.js-Dienst an (nur Apache mit PHP).
                
                Geben Sie hier Ihren ECMAScript 2020-Quellcode für Ihren Express.js-Server ein:`,
                "path": "static/data/aufgabe6/6.3.1.js",
                "showAsCode": 0},
                               
            ]
        },
     ]
    };
 var aufgabe7 = {
     "aufgabe": "Aufgabe 7",
     "nr": 7,
     "unteraufgaben": [
        {
            "unteraufgabenNr":1,
            "title":"7.1. Web-Komponenten erstellen",
            "aufgabenMitLoesung": [
                {"text": `Verpacken Sie Ihre Lösungen aus LE04 in wiederverwendbare Web-Komponenten.`,
                "path": "static/data/aufgabe7/7.1.html",
                "showAsCode": 1},
                               
            ]
        },
        {
            "unteraufgabenNr":2,
            "title":"7.2. LitElement Menü-Komponente",
            "aufgabenMitLoesung": [
                {"text": `Schreiben Sie mit LitElement eine flexible Menü-Komponente, die sich sowohl für horizontale als auch für vertikale Menüs eignet, wie sie sie in Ihrem WWW-Navigator gebrauchen könnten.

                Geben Sie hier den vollständigen Quellcode Ihrer Menü-Komponente ein:`,
                "path": "static/data/aufgabe7/7.2.html",
                "showAsCode": 1},
                               
            ]
        },
        {
            "unteraufgabenNr":3,
            "title":"7.3. LitElement WWW-Navigator",
            "aufgabenMitLoesung": [
                {"text": `Zerlegen Sie Ihren WWW-Navigator (aus Ü5.4) in wiederverwendbare Web-Komponenten. Implementieren Sie diese mit LitElement.

                Geben Sie hier den vollständigen Quellcode Ihres WWW-Navigators ein:`,
                "path": "static/data/aufgabe7/7.3.html",
                "showAsCode": 1},
                               
            ]
        },
     ]
    };
 var aufgabe8 = {
     "aufgabe": "Aufgabe 8",
     "nr": 8,
     "unteraufgaben": [
        {
            "unteraufgabenNr":1,
            "title":"8.1. PWA (10 Punkte)",
            "aufgabenMitLoesung": [
                {"text": `Wandeln Sie Ihren LitElement WWW-Navigator aus Aufgabe 7.3 in eine Progressive Web App um.`,
                "path": "static/data/aufgabe8/8.1.1.html",
                "showAsCode": 0},
                {"text": "PWA Navigator",
                  "path": "static/data/aufgabe8/aufgabe8/index.html" ,
                  "showAsCode": 1 }         
            ]
        },
     ]
    };
    var aufgabe9 = {
        "aufgabe": "Aufgabe 9",
        "nr": 9,
        "unteraufgaben": [
            {
                "unteraufgabenNr":1,
                "title":"Aufgabe 9.1: Komponente in Vue.js",
                "aufgabenMitLoesung": [
                    {"text": `Schreiben Sie eine Vue.js Single File Component mit einem Text-Eingabefeld und 3 Ausgabefeldern, in denen man während des Tippens sehen kann, (a) wie viele Buchstaben (b) wie viele Leerzeichen und (c) wie viele Worte man in das Text-Eingabefeld bereits eingegeben hat.

                    Betten Sie Ihre Komponente in eine Webseite zweimal ein und testen Sie, ob beide Komponenten unabhängig voneinander sind. Geben Sie Ihre Vue.js Single File Component hier ein:`,
                    "path": "static/data/aufgabe9/9.1.1.js",
                    "showAsCode": 0},
                    {"text": "Geben Sie die Webseite, auf der Sie Ihre Komponente mehrfach testen, hier ein:",
                      "path": "static/data/aufgabe9/9.1.2.txt" ,
                      "showAsCode": 0     
                }
                           
                ]
            },
            {
                "unteraufgabenNr":2,
                "title":"Aufgabe 9.2: Menü-Komponente",
                "aufgabenMitLoesung": [
                    {"text": `Schreiben Sie eine möglichst flexible Vue.js Single File Component für Menüs und wenden Sie diese in derselben Webseite zweimal an, einmal horizontal, das andere Mal vertikal.

                    Geben Sie die Inhalte aller Dateien Ihrer Lösung inkl. JS-Quelltext hintereinander ein. Schreiben Sie vor jede Datei deren Dateiname:`,
                    "path": "static/data/aufgabe9/9.2.1.txt",
                    "showAsCode": 0},
                    
                           
                ]
            },
            {
                "unteraufgabenNr":3,
                "title":"Aufgabe 9.3: Vue.js WWW-Navigator",
                "aufgabenMitLoesung": [
                    {"text": `Schreiben Sie Ihren WWW-Navigator als SPA in Vue.js (optional: mit Vue Router und/oder mit Vuex als State Manager).`,
                    "path": "static/data/aufgabe9/9.3.1.txt",
                    "showAsCode": 0},
                    
                           
                ]
            },
        ]
    };
 var aufgabe10 = {
     "aufgabe": "Aufgabe 10",
     "nr": 10,
     "unteraufgaben": [
        {
            "unteraufgabenNr":1,
            "title":"10.1. WebAssembly-Modul von Hand erstellen",
            "aufgabenMitLoesung": [
                {"text": `Erstellen Sie ein WebAssembly-Modul für den größten gemeinsamen Teiler von Hand in WAT, kompilieren Sie diesen mit wat2wasm nach WASM und testen Sie Ihr Modul durch Aufruf mit Parametern aus dem Bereich von 1 bis 100.

                Geben Sie hier Ihren WAT-Code für ggT ggT( x: i32, y: i32 ): i32 ein:`,
                "path": "static/data/aufgabe10/10.1.1.html",
                "showAsCode": 0},
                
                       
            ]
        },
     ]
    };



export default
{
   "aufgaben": [
       {
           "aufgabe": "Home",
           "nr": 0,
           "contentToShow": "<h2>Willkommen auf der Dokumentationswebsite der Veranstaltung Webengineering WS2020 von Alexander Müsgen</h2><p> Diese Homepage ist eine Vue Single Page Application, die die von mir bearbeiteten Übungen der Veranstaltung Webengineering dokumentiert.</p><a href='https://lea.hochschule-bonn-rhein-sieg.de/goto.php?target=crs_796675&client_id=db_040811' >Zur Webengineering Veranstaltung geht es hier lang</a>"
       },
         aufgabe1,
         aufgabe2,
         aufgabe3,
         aufgabe4,
         aufgabe5,
         aufgabe6,
         aufgabe7,
         aufgabe8,
         aufgabe9,
         aufgabe10
    ]
}












